const express = require('express')
const app = express()

const routes = {
    campeao :  require("./api/routers/campeao"),
    item :     require("./api/routers/item"),
    mapa :     require("./api/routers/mapa"),
    modoJogo : require("./api/routers/modoJogo"),
    monstro :  require("./api/routers/monstro")
}

app.use('/campeao', routes.campeao)
app.use('/item', routes.item)
app.use('/mapa', routes.mapa)
app.use('/modoJogo', routes.modoJogo)
app.use('/monstro', routes.monstro)

app.listen(8080, () => {
    console.log("Iniciado com sucesso!")
})