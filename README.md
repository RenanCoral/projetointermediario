## Endpoints

### Campeão

#### Descrição
Retorna informações sobre cada campeão.

#### URI Padrão
> http://localhost:8080/campeao

#### Atributos
| campo | tipo | descrição |
| -- | -- | -- |
| _id | String | Identificador único do campeão |
| nome | String | Nome do campeão |
| tipo | String | Tipo do campeão |

#### Métodos

| Método | URI | descrição |
| -- | -- | -- |
| GET | `/campeao` | Retorna todos os campeões cadastrados |
| GET | `/campeao/<id>` | Retorna um campeão com base no `id` |
| POST | `/campeao` | Insere um novo campeão |
| PUT | `/campeao/<id>` | Atualiza o campeão com base no `id` |
| DELETE | `/campeao/<id>` | Deleta o campeão com base no `id` |

#### Filtros

| atributo | descrição |
| -- | -- |
| `?limit=<numero>` | Limita a quantidade de dados de uma requisição |


## Endpoints

### Item

#### Descrição
Retorna informações sobre cada item.

#### URI Padrão
> http://localhost:8080/item

#### Atributos
| campo | tipo | descrição |
| -- | -- | -- |
| _id | String | Identificador único do item |
| nome | String | Nome do item |
| valor | Number | Valor do item |
| descricao | String | Descrição do item |


#### Métodos

| Método | URI | descrição |
| -- | -- | -- |
| GET | `/item` | Retorna todos os itens cadastrados |
| GET | `/item/<id>` | Retorna um item com base no `id` |
| POST | `/item` | Insere um novo item |
| PUT | `/item/<id>` | Atualiza o item com base no `id` |
| DELETE | `/item/<id>` | Deleta o item com base no `id` |

#### Filtros

| atributo | descrição |
| -- | -- |
| `?limit=<numero>` | Limita a quantidade de dados de uma requisição |


## Endpoints

### Mapa

#### Descrição
Retorna informações sobre cada mapa.

#### URI Padrão
> http://localhost:8080/mapa

#### Atributos
| campo | tipo | descrição |
| -- | -- | -- |
| _id | String | Identificador único do mapa |
| nome | String | Nome do mapa |
| quantidadeLanes | Number | Quantidade de lanes existentes no mapa |

#### Métodos

| Método | URI | descrição |
| -- | -- | -- |
| GET | `/mapa` | Retorna todos os mapas cadastrados |
| GET | `/mapa/<id>` | Retorna um mapa com base no `id` |
| POST | `/mapa` | Insere um novo mapa |
| PUT | `/mapa/<id>` | Atualiza o mapa com base no `id` |
| DELETE | `/mapa/<id>` | Deleta o mapa com base no `id` |

#### Filtros

| atributo | descrição |
| -- | -- |
| `?limit=<numero>` | Limita a quantidade de dados de uma requisição |


## Endpoints

### Modo de Jogo

#### Descrição
Retorna informações sobre cada modo de jogo.

#### URI Padrão
> http://localhost:8080/modoJogo

#### Atributos
| campo | tipo | descrição |
| -- | -- | -- |
| _id | String | Identificador único do modo de jogo |
| nome | String | Nome do modo de jogo |
| descricao | String | Breve descrição de como o modo funciona |

#### Métodos

| Método | URI | descrição |
| -- | -- | -- |
| GET | `/modoJogo` | Retorna todos os modos de jogo cadastrados |
| GET | `/modoJogo/<id>` | Retorna um modo de jogo com base no `id` |
| POST | `/modoJogo` | Insere um modo de jogo |
| PUT | `/modoJogo/<id>` | Atualiza o modo de jogo com base no `id` |
| DELETE | `/modoJogo/<id>` | Deleta o modo de jogo com base no `id` |

#### Filtros

| atributo | descrição |
| -- | -- |
| `?limit=<numero>` | Limita a quantidade de dados de uma requisição |


## Endpoints

### Monstros

#### Descrição
Retorna informações sobre cada monstro.

#### URI Padrão
> http://localhost:8080/monstro

#### Atributos
| campo | tipo | descrição |
| -- | -- | -- |
| _id | String | Identificador único do monstro |
| nome | String | Nome do monstro |
| descricao | String | Descrição do monstro |

#### Métodos

| Método | URI | descrição |
| -- | -- | -- |
| GET | `/monstro` | Retorna todos os monstros cadastrados |
| GET | `/monstro/<id>` | Retorna um monstro com base no `id` |
| POST | `/monstro` | Insere um monstro |
| PUT | `/monstro/<id>` | Atualiza o monstro com base no `id` |
| DELETE | `/monstro/<id>` | Deleta o monstro com base no `id` |

#### Filtros

| atributo | descrição |
| -- | -- |
| `?limit=<numero>` | Limita a quantidade de dados de uma requisição |