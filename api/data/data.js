const mongoose = require('mongoose')

var url = "mongodb://localhost:27017/jpw"
var options = {useNewUrlParser: true, useUnifiedTopology: true}

mongoose.connect(url, options).catch((err) => {
    console.log(err)
})

mongoose.connection.on('connected', () => {
    console.log("Mongoose conectado ao mongodb!")
})

module.exports = mongoose