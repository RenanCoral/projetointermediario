const mongoose = require('../data/data.js')

const itemSchema = mongoose.Schema({
    'nome': String,
    'valor': Number,
    'descricao': String
})

const Item = mongoose.model('Item', itemSchema)

module.exports = Item;
 
