const mongoose = require('../data/data.js')

const monstroSchema = mongoose.Schema({
    'nome': String,
    'descricao': String
})

const Monstro = mongoose.model('Monstro', monstroSchema)

module.exports = Monstro;
 
