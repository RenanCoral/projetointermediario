const mongoose = require('../data/data.js')

const mapaSchema = mongoose.Schema({
    'nome': String,
    'quantidadeLanes': Number
})

const Mapa = mongoose.model('Mapa', mapaSchema)

module.exports = Mapa;
 
