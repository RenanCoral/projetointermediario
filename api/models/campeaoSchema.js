const mongoose = require('../data/data.js')

const campeaoSchema = mongoose.Schema({
    'nome': String,
    'tipo': String
})

const Campeao = mongoose.model('Campeao', campeaoSchema)

module.exports = Campeao;
 
