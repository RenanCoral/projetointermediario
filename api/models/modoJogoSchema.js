const mongoose = require('../data/data.js')

const modoSchema = mongoose.Schema({
    'nome': String,
    'descricao': String
})

const Modo = mongoose.model('Modo', modoSchema)

module.exports = Modo;
 
