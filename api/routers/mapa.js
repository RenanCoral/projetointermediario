const express = require('express')
const router = express.Router()
const Mapa = require('../models/mapaSchema')

router.use(express.json())

router.get("/", (req, res) => {
    var limit = Number(req.query.limit)
    Mapa.find().limit(limit).then((doc) => {
        res.status(200).json(doc)
    }).catch(function(err){
        console.error(err)
        res.status(400).json({ "erro": "Erro ao fazer busca" })
    })
})

router.get("/:id", (req, res) => {
    Mapa.findById(req.params.id, (err, doc) => {
        if(err) res.status(400).json({ "erro": "Erro ao fazer busca" })
        res.status(200).json(doc)
    })
})

router.post("/", (req, res) => {
    var mapa = new Mapa(req.body)

    Mapa.save((err, doc) => {
        if(err) res.status(400).json({ "erro": "Não foi possivel inserir os dados" })
        res.status(200).json(doc)
    })
})

router.put("/:id", (req, res) => {

    Mapa.findByIdAndUpdate(req.params.id, req.body, (err, doc) => {
        if(err) res.status(400).json({ "erro": "Erro ao alterar dados" })
        res.status(200).json(doc)
    })
})

router.delete("/:id", (req, res) => {

    Mapa.findByIdAndDelete(req.params.id, (err, doc) => {
        if(err) res.status(400).json({ "erro": "Erro ao excluir dados" })
        res.status(200).json(doc)
    })
})

module.exports = router
